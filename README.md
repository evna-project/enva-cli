# enva-cli
enva-cli is a tool for creating files, folders or event projects due to a recipe!

# Api

Enva cli is capable of doing things that we do every day, again and agian:)

## Basic commands

Each command should consist of a primary command and some subsequent argument(s):
```
$ enva <command> <...arguments>
```

### Configuration file

```javascript
{
  // environmental variables that will be passed to the enva's functions
  environments: { 
    sth: "sth"
  },
  commands: {

  },
  plugins: {
    fileCreator: {
      // some config
    }
  },
  extends: [
    'enva'
  ]
}
```

### Create commands

```javascript
{
  commands: {
    'commandName': 'index.js',
    'commandName2': 'simple.bash'
  }
}
```

After that:

```
$ enva commandName arg1
```
index.js

```javascript
console.log(enva.arguments[0]) // arg1
```

### Creating file/folder/project

**I highly recomment to add this as a plugin, not to the core of the enva.**

To create files(or folder or project), we use this command.

```
$ enva c <file/folder/project name>
```

After entering this command enva should look for a configuration file to see what is the file and how to create it. Configuration file can be either global or local.

Each file can consist of multiple variables or conditions.

For example, if we want to create a file called a.vue, a.html can be sth like this:

```html

<html>
  <head>
    <title>{{ title }}</title>
  </head>
  {{ if ($sth) }}
  <div>
  </div>
  {{ else }}
  <div>
  </div>
  {{ end if }}
</html>

```
After you enter `enva c a`, enva will ask you the variables and at the end give you the file you want.

Briefly, files can have these features:

* Variables
  * Vars can be optional and have default value
* Conditions
* Loops